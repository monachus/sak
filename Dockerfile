FROM leodotcloud/swiss-army-knife:dev

RUN apt-get -qq update && apt-get -qq -y install \
    git openssh-client telnet netcat mysql-client unzip && \
  rm -fr /var/lib/apt/lists/* 
